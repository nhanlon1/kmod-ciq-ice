# CIQ Module for Latest Intel ICE Drivers

This is a CIQ package to make the latest Intel ICE drivers easily installable on Rocky Linux.

## Summary

This package makes it easy to compile these drivers against any Rocky Linux kernel version you set in the spec file.  Driver packages are named according to the kernel it was compiled for, and the packages are independently installable - installing one doesn't mean upgrading/erasing another.  This way a customer simply installs the matching driver package against any and all kernels they have installed on their system.

Each commit in this Git repository should reflect a new supported kernel, and the commits should be tagged with the kernel version supported for ease of navigation.

<br /><br />


## Naming Strategy
Package files will have funny names, because multiple must be installable at the same time (one for each kernel version).

To support this, the NAME of the RPM is always:  "kmod-ciq-ice+<KERNEL_VERSION>"

The VERSION of the RPM is the latest upstream from Intel (currently 1.8.9:  https://sourceforge.net/projects/e1000/ )

RELEASE of the RPM is incremented to indicate any extra patches that CIQ has added in SOURCES/  (like a patch for Rocky 8.6 support)


<br /><br />


## Symbolic Link + File Strategy
Because these packages must be installable alongside each other, their files are kept in separate directories, labelled after the kernel they are built against.

For example, instead of **/lib/firmware/updates/intel/ice/**, the directory will be **/lib/firmware/updates/intel/ice+4.18.0-372.13.1.el8_6/** . This way different packages with the same files will not conflict with each other.

There is a post-install scriptlet included that performs a symbolic link to the relevant folders, making the most recently installed version the "active" one.  This doesn't interfere with the actual driver, because the .ko driver file is always kept separate per-kernel already: **/lib/modules/<KERNEL_VERSION>/updates/drivers/net/ethernet/intel/ice/ice.ko**


Things like firmware files and the manpage will always have a "default" symlink though.  Example:  /usr/share/man/man7/ice.7.gz  ->  /usr/share/man/man7/ice+4.18.0-372.9.1.el8.7.gz

<br /><br />


## Building
Building is pretty straightforward:

- Checkout this repository, and your desired kernel version tag
- Download the ice-1.8.9.tar.gz file into SOURCES/ from upstream or a lookaside location
- Build the SRPM from the main directory:  `rpmbuild -v -bs SPECS/kmod-ciq-ice.spec --define "_topdir ./"`
- Build the RPM with mock:  `mock -v --resultdir ./ --isolation simple  SRPMS/kmod-ciq-ice+4.18.0-372.13.1.el8_6-1.8.9-1.el8.src.rpm`  


**NOTE:  When building against an older kernel (like from Rocky 8.5), your Mock config needs to point to the Rocky Vault.  The necessary kernel-devel and kernel-headers packages will not be in the current release**

<br /><br />


## Installing

Installing is as simple as `dnf install  kmod-ciq-ice+<DESIRED_KERNEL_VERSION>`.

To help customers, here is a simple script that will install the kmod for every kernel version they have on their system:


```
#!/bin/bash

# Simple script to list all installed "kernel" packages, and ensure the matching kmod-ciq-ice driver is installed

PKGS=""
for i in `rpm -q --qf "%{VERSION}-%{RELEASE}\n" kernel`; do
  _tmp=`echo ${i} | sed 's/kernel\-//' `
  PKGS="${PKGS}  kmod-ciq-ice+${i}"
done


dnf install ${PKGS}
```

<br /><br />
